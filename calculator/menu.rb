require_relative 'operations'

module Calculator
  include ExtraOperations
  class Menu
    def initialize
      menu = ''
      puts menu 
      puts '----------------------------------------'
      puts'|        Bem vindo a calculadora       |'
      puts '----------------------------------------'
      puts '|                                      |'
      puts '|  Selecione uma das seguintes opções  |'
      puts '|                                      |'
      puts '|       1- Média preconceituosa        |'
      puts '|       2- Sem números                 |'
      puts '|       3- Filtrar Filmes              |'
      puts '|       4- Tipos de Força              |'
      puts '|       5- Distancia de Fases          |'
      puts '|       0- Sair                        |'
      puts '----------------------------------------'
      print "\n"
      print 'Opção: '
      
      option = gets.chomp.to_f
      print "\n"
      objeto=Operations.new
      
      case option 
      when 1
        puts "Digite os nomes lista negra separando-os por espaço ' ': "
        blacklist = gets.chomp
        print "\n"
        puts "Digite as notas: EX:{'nome_do_aluno1':nota,'nome_do_aluno2':nota2}"
        grades = gets.chomp
        print "\n"
        objeto.biased_mean(grades, blacklist)
      
      when 2
        puts "Digite os numeros separando-os por espaço ' ':"
        print "\n"
        numbers = gets.chomp
        objeto.no_integers(numbers)

      when 3
        puts "Digite os generos desejados separando-os por espaço ' ', EX: Fantasy Comedy"
        genres = gets.chomp
        puts "Digite o ano:"
        year = gets.chomp.to_i
        puts "Aperte 1 se quiser ver a sinopse e se não quiser digite qualquer outro numero:"
        sinopse = gets.chomp.to_i
        print "\n"
        films = objeto.filter_films(genres, year, sinopse)          
        print "-----------------Filmes Encontrados---------------------\n"
        puts films
        print "--------------------------------------------------------\n"

      when 4
        puts "Digite o valor da carga 1: (pode ser positivo, negativo ou nulo)"
        carga1 = gets.chomp.to_f
        puts "Digite o valor da carga 2: (pode ser positivo, negativo ou nulo)"
        carga2 = gets.chomp.to_f
        puts "Digite a distancia (qualquer numero acima de zero):"
        distancia = gets.chomp.to_f
        resultado = objeto.forca(carga1, carga2, distancia)
        puts resultado

      when 5
        puts "Digite a distancia entre as duas antenas (qualquer numero acima de zero):"
        distancia = gets.chomp.to_f
        puts "Digite o comprimento de onda da onda incidente (qualquer numero acima de zero):"
        comprimento = gets.chomp.to_f
        puts "Digite o angulo em graus EX: 30,60...."
        graus = gets.chomp.to_f
        resultado = objeto.fases(distancia, comprimento, graus)
        puts resultado

      when 0
        exit
      else
        menu= 'Opção inválida'
      end
    end
  end
end
