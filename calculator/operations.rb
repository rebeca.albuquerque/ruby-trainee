require_relative '../extra_operations'
require 'net/http'
require 'json'

module Calculator
  class Operations
    include ExtraOperations
  
    def biased_mean(grades, blacklist)
      
      gradesz = JSON.parse(grades)
      hashblack = blacklist.split
      selection = gradesz.select do |a|
        not hashblack.include? a
      end
      
      total = 0
      selection.each do |key, value|
        total += value
      end
      tam = selection.length.to_f
      media = total/(tam)
      print "\n"
      print "A média é "
      print media
      print "\n"
    end
  
    def no_integers(numbers)    
      array = numbers.split
      array.each do |num|
        last = num[-2..-1]
        case last 
        when '00'
          print 'Sim '
        when '25' 
          print 'Sim '   
        when '50'
          print 'Sim ' 
        when '75' 
          print 'Sim '
        else
          print 'Não '
        end
      end
      print "\n"
    end
  
    def filter_films(genres, year, sinopse)
      genres = genres.split
      films = get_films[:movies]
      results = []
      
      films.each do |film|
        if((genres & film[:genres]) == genres && film[:year].to_i>= year)
          results.push(film[:title] + " " + film[:year])
          if(sinopse == 1)
          results.push("Sinopse: " + film[:plot])
          end
        end
      end      


      return results

    end
    
    private
  
    def get_films
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json'
      uri = URI(url)
      response = Net::HTTP.get(uri)
      return JSON.parse(response, symbolize_names: true)
    end
  end
end
