module ExtraOperations
    def forca(carga1, carga2, distancia)
        
        constante_dieletrica_dovacuo = 8.9876*(10**9) #constante dieletrica do vacuo
        forcaN = (constante_dieletrica_dovacuo*carga1*carga2)/(distancia**2)#lei de coulomb
        if (forcaN < 0)
            forcaN = forcaN*(-1) #para não retornar uma força negativa
        end
        print "\n"

        if (distancia > 0)
            if forcaN == 0
                puts "A força é nula"
            end
            #lei da atração, cargas diferentes se atraem e cargas iguais se repelem
            if carga1<0 && carga2>0
                resultado = "O valor da força é " + forcaN.to_s + " N e a força é de atração"
            end
            if carga1 >0 && carga2<0
                resultado = "O valor da força é " + forcaN.to_s + " N e a força é de atração"
            end
            if carga1<0 && carga2<0 
                resultado = "O valor da força é " + forcaN.to_s + " N e a força é de repulsão"
            end
            if carga1>0 && carga2>0
                resultado = "O valor da força é " + forcaN.to_s + " N e a força é de repulsão"
            end
        else
            if distancia == 0
                resultado = "não é possivel ter distancia nula"
            end
            if distancia < 0
                resultado = "não é possivel ter distancia negativa"
            end
        end

        return resultado  
    end

    def fases(distancia, comprimento, graus)
        radiano = graus/(180*Math::PI) #conversão de graus para radiano
        fase = (2*Math::PI)/(comprimento*distancia*Math.sin(radiano)); #calculo da distancia de fases (isso é uma fórmula pronta, conceito físico)
        print "\n"
        if (distancia > 0 && comprimento > 0)
        resultado = "A distancia de fases é " + fase.to_s
        else 
            resultado = "Digite valores acima de zero para a distancia e o comprimento"
        end

        return resultado 
    end
end
